// let port = '8889'
let port = '80'
let domain = 'vianco.neonstudio.us'
let protocol = 'http://'
let config = {
    setPort: (_port) => { port = _port },
    getHost: () => { return (protocol + domain + ":" + port + '/') }
}
export default config