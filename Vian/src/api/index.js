import { Dimensions } from 'react-native';
_token = '0b8d5a83-d655-4309-ace0-0468c6bc5dc0'
export default api = {
    colorPrimary() {
        return 'rgb(0,143,212)';
    },
    getToken: () => {
        return _token;
    },
    // nue dang nhap duoc thi luu lai token
    setToken: (token) => {
        if (token) _token = token;
    }
}