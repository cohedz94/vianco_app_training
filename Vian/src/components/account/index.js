import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View, TouchableOpacity, Image, DrawerLayoutAndroid, FlatList,
} from 'react-native';

import Draw from 'react-native-drawer';
// import { Icon } from 'native-base';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/Ionicons'

import api from '../../api/index';
import { PagerTabIndicator, IndicatorViewPager, PagerTitleIndicator, PagerDotIndicator } from 'rn-viewpager';
export default class Account extends Component {

    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={{ height: 50, flexDirection: 'row', alignItems: 'center', backgroundColor: '#048fd2' }}>
                    <TouchableOpacity
                        onPress={() => { Actions.drawerOpen() }}
                    >
                        <Image source={require('../../image/menu.png')} style={{ marginLeft: 5, height: 30, width: 30 }} />
                    </TouchableOpacity>
                    <Text style={{
                        flex: 1,
                        fontSize: 15, color: '#fff', flex: 1,
                        textAlign: 'center'
                    }}>
                        Quản lý tài khoản
                </Text>
                    <View style={{ flexDirection: 'row', }}>
                        <TouchableOpacity>
                            <Image style={{ height: 25, width: 30, padding: 5 }} source={require('../../image/search.png')} />
                        </TouchableOpacity>

                        <Image style={{ height: 25, width: 30, marginRight: 10 }} source={require('../../image/cart.png')} />
                    </View>
                </View>
                <View style={{ flex: 1, backgroundColor: 'white' }}>
                    <View style={{ height: 50, justifyContent: 'center', borderBottomColor: 'grey', borderBottomWidth: 1, }}>
                        <TouchableOpacity onPress={() => Actions.inforAccount()}>
                            <Text style={{ marginLeft: 30, fontSize: 15 }}>Thông tin tài khoản</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.btn}>
                        <TouchableOpacity onPress={() => Actions.oder()}>
                            <Text style={styles.txtTou}>Quản lý đơn hàng</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.btn}>
                        <TouchableOpacity onPress={() => Actions.inforAccount()}>
                            <Text style={styles.txtTou} >Thông báo</Text>
                            
                        </TouchableOpacity>
                    </View>
                    <View style={styles.btn}>
                        <TouchableOpacity onPress={() => Actions.inforAccount()}>
                            <Text style={styles.txtTou}>Sản phẩm đã xem</Text>
                        </TouchableOpacity>
                    </View>
                    {/* <View style={styles.btn}>
                        <TouchableOpacity onPress={() => Actions.inforAccount()}>
                            <Text style={styles.txtTou}>Sản phẩm yêu thích</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.btn}>
                        <TouchableOpacity onPress={() => Actions.inforAccount()}>
                            <Text style={styles.txtTou}>Sản phẩm đã mua</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.btn}>
                        <TouchableOpacity onPress={() => Actions.inforAccount()}>
                            <Text style={styles.txtTou}>Nhận xét của tôi</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.btn}>
                        <TouchableOpacity onPress={() => Actions.inforAccount()}>
                            <Text style={styles.txtTou}>Mã ưu đãi của tôi</Text>
                        </TouchableOpacity>
                    </View> */}
                </View>

            </View>
        );
    }

}
const styles = StyleSheet.create({
    btn: {
        height: 50, justifyContent: 'center', borderBottomColor: 'grey', borderBottomWidth: 1
    },
    txtTou: {
        marginLeft: 30,

    }
});
