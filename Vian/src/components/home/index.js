import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View, TouchableOpacity, Image, DrawerLayoutAndroid
} from 'react-native';

import Draw from 'react-native-drawer';
// import { Icon } from 'native-base';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/Ionicons'

import api from '../../api/index';
import Product from '../product';
import { sale } from '../sale';
import  ProductNew from '../productNews';
import { promotion } from '../promotion';

import { PagerTabIndicator, IndicatorViewPager, PagerTitleIndicator, PagerDotIndicator } from 'rn-viewpager';
export default class Home extends Component {

    _renderTitleIndication() {
        return (
            <PagerTitleIndicator

                itemStyle={{ backgroundColor: 'white', }}
                itemTextStyle={{ color: 'grey' }}
                selectedBorderStyle={{ backgroundColor: api.colorPrimary() }}
                selectedItemTextStyle={{ color: api.colorPrimary(), fontSize: 13 }}
                titles={['Sản phẩm', 'Hàng mới về', 'Giảm giá', 'Khuyến mãi']}
            >
            </PagerTitleIndicator>
        )
    }
    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={{ height: 50, flexDirection: 'row', alignItems: 'center', backgroundColor: '#048fd2' }}>

                    <TouchableOpacity style={{}}
                        onPress={() => { Actions.drawerOpen() }}
                    >
                        <Image source={require('../../image/menu.png')} style={{ marginLeft: 5, height: 30, width: 30 }} />
                    </TouchableOpacity>
                    <Text style={{
                        flex: 2,
                        fontSize: 16, color: '#fff', flex: 1,
                        textAlign: 'center'
                    }}>
                        Trang chủ
                    </Text>
                    <View style={{ flexDirection: 'row', }}>
                        <TouchableOpacity>
                            <Image style={{ height: 25, width: 30, padding: 5 }} source={require('../../image/search.png')} />
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => { Actions.carts() }} >
                            <Image style={{ height: 25, width: 30, marginRight: 10 }} source={require('../../image/cart.png')} />
                        </TouchableOpacity>
                    </View>
                </View>
                <IndicatorViewPager
                    style={{ flex: 1, flexDirection: 'column-reverse' }}
                    indicator={this._renderTitleIndication()}>
                    <View>
                        <Product/>
                    </View>
                    <View>
                        <ProductNew/>
                    </View>
                    {sale()}
                    {promotion()}
                </IndicatorViewPager>
            </View>
        );
    }
}

