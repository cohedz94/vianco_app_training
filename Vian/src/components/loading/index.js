import React, { Component } from 'react';
import { View, Image, StyleSheet } from 'react-native';
import { Actions,ActionConst } from 'react-native-router-flux';

class Loading extends Component {
  // componentDidMount = () => {
  //   setTimeout(function(){
  //     Actions.home({
  //       type:ActionConst.RESET
  //     })
  //   }, 1000)
  // }
  
  render() {
    return (
      <View style={styles.container}>
        <Image
            source={require('../../image/vianco.jpg')}
            style={{width:100, height:100}}>
        </Image>
      </View>
    )
  }
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'white'
    }
})

export default Loading;