

import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View, TouchableOpacity, Image
} from 'react-native';
import { Icon } from 'native-base';
import api from '../../api/index';
import Login from './login';
import Register from './register';
import { PagerTabIndicator, IndicatorViewPager, PagerTitleIndicator, PagerDotIndicator } from 'rn-viewpager';
export default class Sign extends Component {
    _renderTitleIndication() {
        return (
            <PagerTitleIndicator
                itemStyle={{ backgroundColor: 'white' }}
                itemTextStyle={{ color: 'grey' }}
                selectedBorderStyle={{ backgroundColor: api.colorPrimary() }}
                selectedItemTextStyle={{ color: api.colorPrimary() }}
                titles={['Đăng nhập', 'Đăng ký']}
            >
            </PagerTitleIndicator>
        )
    }
    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', backgroundColor: 'steelblue' }}>
                    <TouchableOpacity>
                        <Image source={require('../../image/close.png')} style={{ marginLeft: 5, height: 25, width: 25 }} />
                    </TouchableOpacity>
                    <Text style={{ marginLeft: 20, fontSize: 16, color: '#fff', flex: 1 }}>
                        Đăng nhập/đăng ký
                    </Text>
                </View>
                <IndicatorViewPager
                    style={{ flex: 9, flexDirection: 'column-reverse' }}
                    indicator={this._renderTitleIndication()}>
                    <View>
                        <Login />
                    </View>
                   <View>
                       <Register />
                   </View>
                </IndicatorViewPager>
            </View>
        );
    }
}

