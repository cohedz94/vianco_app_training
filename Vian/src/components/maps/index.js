import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View, TouchableOpacity, Image, DrawerLayoutAndroid, ScrollView, FlatList, TextInput
} from 'react-native';
import Draw from 'react-native-drawer';
import { Actions } from 'react-native-router-flux';
import Icon from 'native-base';
import { PagerTabIndicator, IndicatorViewPager, PagerTitleIndicator, PagerDotIndicator } from 'rn-viewpager';
export default class Maps extends Component {

    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={{ height: 50, flexDirection: 'row', alignItems: 'center', backgroundColor: '#048fd2' }}>
                    <TouchableOpacity
                        onPress={() => { Actions.drawerOpen() }}
                    >
                        <Image source={require('../../image/menu.png')} style={{ marginLeft: 5, height: 30, width: 30 }} />
                    </TouchableOpacity>
                    <Text style={{
                        flex: 1,
                        fontSize: 15, color: '#fff', flex: 1,
                        textAlign: 'center'
                    }}>
                        Bản đồ
                </Text>
                    <View style={{ flexDirection: 'row', }}>
                        <TouchableOpacity>
                            <Image style={{ height: 25, width: 30, padding: 5 }} source={require('../../image/search.png')} />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => { Actions.carts() }} >
                            <Image style={{ height: 25, width: 30, marginRight: 10 }} source={require('../../image/cart.png')} />
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{ flex: 1, backgroundColor: 'white' }}>
                    <View style={{ flexDirection: 'row', height: 40, borderColor: 'grey', borderWidth: 1 }}>
                        <View style={{ flex: 1, borderRightWidth: 1, borderRightColor: 'gray' }}>
                            <TextInput placeholder="Tìm kiếm" underlineColorAndroid="transparent"
                            />
                        </View>

                        <TouchableOpacity style={{ flex: 1, alignItems: 'center', justifyContent: 'center', }} >
                            <Text style={{ textAlign: 'center', color: 'grey' }}>Khu vực </Text>
                            
                        </TouchableOpacity>

                    </View>
                    <View>

                    </View>
                </View>

            </View>
        );
    }

}
const styles = StyleSheet.create({
    btn: {
        height: 50, justifyContent: 'center', borderBottomColor: 'grey', borderBottomWidth: 1
    },
    txtTou: {
        marginLeft: 30,

    }
});
