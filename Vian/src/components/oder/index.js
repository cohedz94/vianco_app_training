import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View, TouchableOpacity, Image, DrawerLayoutAndroid, FlatList,
} from 'react-native';

import Draw from 'react-native-drawer';
// import { Icon } from 'native-base';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/Ionicons'

import api from '../../api/index';
import { product } from '../product';
import { sale } from '../sale';
import { productNews } from '../productNews';
import { promotion } from '../promotion'
import { PagerTabIndicator, IndicatorViewPager, PagerTitleIndicator, PagerDotIndicator } from 'rn-viewpager';
export default class Oder extends Component {

    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={{ height: 50, flexDirection: 'row', alignItems: 'center', backgroundColor: '#048fd2' }}>

                    <TouchableOpacity style={{}}
                        onPress={() => { Actions.drawerOpen() }}
                    >
                        <Image source={require('../../image/menu.png')} style={{ marginLeft: 5, height: 30, width: 30 }} />
                    </TouchableOpacity>
                    <Text style={{
                        flex: 2,
                        fontSize: 15, color: '#fff',
                        textAlign: 'center'
                    }}>
                        Danh sách đơn hàng
                     </Text>
                    <View style={{ flexDirection: 'row', }}>
                        <TouchableOpacity>
                            <Image style={{ height: 25, width: 30, padding: 5 }} source={require('../../image/search.png')} />
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => { Actions.carts() }} >
                            <Image style={{ height: 25, width: 30, marginRight: 10 }} source={require('../../image/cart.png')} />
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{ flex: 1, backgroundColor: 'white' }}>
                    <View style={{ height: 50, justifyContent: 'center', borderBottomColor: 'grey', borderBottomWidth: 1, }}>
                        <TouchableOpacity  onPress={()=>{ Actions.recentOrder();}}>
                            <Text style={{ marginLeft: 30, fontSize: 15 }}>Các đơn hàng gần nhất</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.btn}>
                        <TouchableOpacity onPress={()=>{ Actions.receivedOrder();}} >
                            <Text style={styles.txtTou}>Đơn hàng tiếp nhận</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.btn}>
                        <TouchableOpacity  onPress={()=>{ Actions.shippedOrder();}}>
                            <Text style={styles.txtTou} >Đơn hàng đang vẫn chuyển</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.btn}>
                        <TouchableOpacity>
                            <Text style={styles.txtTou}>Đơn hàng thành công</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.btn}>
                        <TouchableOpacity >
                            <Text style={styles.txtTou}>Đơn hàng đã hủy</Text>
                        </TouchableOpacity>
                    </View>
                </View>

            </View>
        );
    }

}
const styles = StyleSheet.create({
    btn: {
        height: 50, justifyContent: 'center', borderBottomColor: 'grey', borderBottomWidth: 1
    },
    txtTou: {
        marginLeft: 30,

    }
});
