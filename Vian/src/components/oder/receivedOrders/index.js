import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View, TouchableOpacity, Image, DrawerLayoutAndroid, ScrollView, FlatList, TextInput
} from 'react-native';
import Draw from 'react-native-drawer';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/Ionicons';
import { SearchBar } from 'react-native-elements';
import { PagerTabIndicator, IndicatorViewPager, PagerTitleIndicator, PagerDotIndicator } from 'rn-viewpager';
export default class ReceivedOrder extends Component {

    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={{ height: 50, flexDirection: 'row', alignItems: 'center', backgroundColor: '#048fd2' }}>
                    <TouchableOpacity
                        onPress={() => { Actions.drawerOpen() }}
                    >
                        <Image source={require('../../../image/menu.png')} style={{ marginLeft: 5, height: 30, width: 30 }} />
                    </TouchableOpacity>
                    <Text style={{
                        flex: 1,
                        fontSize: 15, color: '#fff', flex: 1,
                        textAlign: 'center'
                    }}>
                        Đơn hàng đã tiếp nhận
                </Text>
                    <View style={{ flexDirection: 'row', }}>
                        <TouchableOpacity>
                            <Image style={{ height: 25, width: 30, padding: 5 }} source={require('../../../image/search.png')} />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => { Actions.carts() }} >
                            <Image style={{ height: 25, width: 30, marginRight: 10 }} source={require('../../../image/cart.png')} />
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{ flex: 1, backgroundColor: 'white' }}>
                    <View style={{ flexDirection: 'row', height: 40, borderColor: 'grey', borderWidth: 1 }}>
                        <View style={{ flex: 1, borderRightWidth: 1, borderRightColor: 'gray' }}>
                             <TextInput placeholder="Tìm kiếm" underlineColorAndroid="transparent"
                            />
                            {/* <SearchBar
                                onChangeText={someMethod}
                                onClearText={someMethod}
                                placeholder='Tìm kiếm ' /> */}
                        </View>
                        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                            <TouchableOpacity>
                                <Text style={{ textAlign: 'center', color: 'grey' }}>Ngày vận chuyển</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View>
                        <FlatList horizontal={false} data={listOder}
                            style={{}}
                            renderItem={({ item }) =>
                                <View style={{ flexDirection: 'row', borderBottomColor: 'gray', borderBottomWidth: 1, paddingRight: 20, paddingLeft: 20 }}>
                                    <View style={{ flex: 2, margin: 10 }}>
                                        <View style={{ flexDirection: 'row' }}>
                                            <Text >Mã đơn hàng :</Text>
                                            <Text>{item.maDH}</Text>
                                        </View>
                                        <View style={{ flexDirection: 'row' }}>
                                            <Text >Sản phẩm :</Text>
                                            <Text>{item.sanpham}</Text>
                                        </View>
                                        <View style={{ flexDirection: 'row' }}>
                                            <Text >Thành tiền :</Text>
                                            <Text>{item.thanhtien}</Text>
                                        </View>
                                        <View style={{ flexDirection: 'row' }}>
                                            <Text >Ngày đặt :</Text>
                                            <Text>{item.ngaydat}</Text>
                                        </View>
                                        <View style={{ flexDirection: 'row' }}>
                                            <Text >Ngày Nhận :</Text>
                                            <Text>{item.ngaynhan}</Text>
                                        </View>
                                    </View>
                                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                        <TouchableOpacity style={{ height: 30, width: 85, backgroundColor: '#048fd2', justifyContent: 'center', alignItems: 'center' }} onPress={() => { Actions.detailOrder(); }}>
                                            <Text style={{ color: 'white', textAlign: 'center', fontSize: 14 }}> Chi tiết</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity style={{ height: 30, width: 85, backgroundColor: '#048fd2', justifyContent: 'center', alignItems: 'center', marginTop: 10 }}>
                                            <Text style={{ color: 'white', textAlign: 'center', fontSize: 14 }}> Hủy</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            }

                        />
                    </View>
                </View>

            </View>
        );
    }

}
const styles = StyleSheet.create({
    btn: {
        height: 50, justifyContent: 'center', borderBottomColor: 'grey', borderBottomWidth: 1
    },
    txtTou: {
        marginLeft: 30,

    }
});
const listOder = [
    {
        key: 0,
        maDH: 'DH01',
        sanpham: '2 Bóng đèn Led',
        thanhtien: '150 đ',
        ngaydat: '7/11/2017',
        ngaynhan: '7/11/2017',
    },
    {
        key: 1,
        maDH: 'DH01',
        sanpham: '2 Bóng đèn Led',
        thanhtien: '150 đ',
        ngaydat: '7/11/2017',
        ngaynhan: '7/11/2017',
    }, {
        key: 2,
        maDH: 'DH01',
        sanpham: '2 Bóng đèn Led',
        thanhtien: '150 đ',
        ngaydat: '7/11/2017',
        ngaynhan: '7/11/2017',
    }, {
        key: 3,
        maDH: 'DH01',
        sanpham: '2 Bóng đèn Led',
        thanhtien: '150 đ',
        ngaydat: '7/11/2017',
        ngaynhan: '7/11/2017',
    }, {
        key: 4,
        maDH: 'DH01',
        sanpham: '2 Bóng đèn Led',
        thanhtien: '150 đ',
        ngaydat: '7/11/2017',
        ngaynhan: '7/11/2017',
    }, {
        key: 5,
        maDH: 'DH01',
        sanpham: '2 Bóng đèn Led',
        thanhtien: '150 đ',
        ngaydat: '7/11/2017',
        ngaynhan: '7/11/2017',
    },
]
