

import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View, TouchableOpacity, Image, ScrollView, FlatList,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Icon } from 'native-base';
import api from '../../api/index';
import ProductItems from './item';
import ProductNew from '../../components/productNews/index';
import dataService from '../../api/dataService'
import { PagerTabIndicator, IndicatorViewPager, PagerTitleIndicator, PagerDotIndicator } from 'rn-viewpager';
// import StarRating from 'react-native-star-rating';
export default class Product extends Component {

    constructor(props) {
        super(props);
        this.state = {
            listProduct: [],
            listCategory:[],
            listDiscount:[]
        }
        dataService.listProduct(0, 10, 'bestsale', null)
            .then(data => {
                console.log('DATA LIST PRODUCT', data);
                this.setState({ listProduct: data.data });
            });

        dataService.listCategory()
            .then(data => {
                console.log('DATA LIST PRODUCT', data);
                this.setState({ listCategory: data.data });
            });

        dataService.listDiscount(0, 10, 'discount', null)
            .then(data => {
                console.log('DATA discount', data);
                this.setState({ listDiscount: data.data });
            });
    }
    render() {
        return (
            <View style={{ flex: 1, }}>
                <ScrollView>
                    <Text style={{ color: 'grey', padding: 16, backgroundColor: 'white', marginTop: 5, marginBottom: 1 }} >
                        Danh mục nhóm sản phẩm
                    </Text>
                    <FlatList
                        style={{ marginBottom: 3 }}
                        data={this.state.listCategory}
                        horizontal={true}
                        renderItem={({ item }) =>
                            <View style={{
                                backgroundColor: 'white',
                                width: 150,
                                height: 200,
                                padding: 10,
                                justifyContent: 'center',
                                 alignItems: 'center',
                                margin: 1
                            }}>
                                <TouchableOpacity onPress={() => alert(item.image[0])}>
                                    <Image
                                        style={{ width: 50, height: 80 ,marginBottom: 20}}
                                        // source={item.image}
                                        source={{uri:item.image}}
                                    />
                                    <Text style={{ fontSize: 12, color: 'grey'}}>{item.name}</Text>
                                </TouchableOpacity>
                            </View>}>
                    </FlatList>
                    {/* hang moi vef */}
                    <View style={{ padding: 16, backgroundColor: 'white', flexDirection: 'row', justifyContent: 'space-between', marginBottom: 1, }}>
                        <Text style={{ color: 'grey', }}>Hàng mới về</Text>
                        <TouchableOpacity>
                            <Text style={{ color: 'steelblue', textDecorationLine: 'underline' }}>Xem thêm</Text>
                        </TouchableOpacity>
                    </View>
                    <FlatList
                        style={{
                            margin: 1
                        }}
                        horizontal={true}
                        data={this.state.listProduct}
                        renderItem={({ item, i }) =>
                            <TouchableOpacity key={i} onPress={() => Actions.detail()}>
                                <ProductItems
                                    name={item.name}
                                    price={item.showPrice}
                                    uri={item.image[0]}
                                    rate={item.rate}
                                />
                            </TouchableOpacity>}>
                    </FlatList>

                    {/* phần giảm giá */}
                    <View style={{ padding: 16, backgroundColor: 'white', flexDirection: 'row', justifyContent: 'space-between', marginBottom: 1 }}>
                        <Text style={{ color: 'grey' }}>Giảm giá</Text>
                        <TouchableOpacity>
                            <Text style={{ color: 'steelblue', textDecorationLine: 'underline' }}>Xem thêm</Text>
                        </TouchableOpacity>
                    </View>
                    <FlatList
                    style={{marginBottom: 20}}
                        horizontal={true}
                        data={this.state.listDiscount}
                        renderItem={({ item }) =>
                            <TouchableOpacity
                                onPress={() => Actions.detail()}>
                                <ProductItems
                                    name={item.name}
                                    price={item.showPrice}
                                    uri={item.image[0]}
                                    rate={item.rate}
                                />
                            </TouchableOpacity>}>
                    </FlatList>
                </ScrollView>
            </View>
        );
    }
}
