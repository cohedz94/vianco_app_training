import React, { Component } from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';
import { Card } from 'native-base';

// import StarRating from 'react-native-stars-rating';

export default class ProductItems extends Component {
    render() {
        let { name, price, uri, rate } = this.props; // <=> a = this.props.a, b = this.props.b
        return (
            <View style={styles.cardItems}>
                <View  style={{flex:4}}>
                    <Image
                        resizeMode='contain'
                        style={{  width: 80,height:75,marginTop:5, backgroundColor: 'white', alignSelf: 'center' ,padding: 50}}
                        source={{ uri: uri }}
                    />
                </View>
                <View style={{ flex:2,justifyContent: 'center', marginLeft:10}}>
                    <Text style={{ color: 'grey', fontSize: 11 }}>{name}</Text>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Text style={{ color: '#f8c21d', fontSize: 11 }}>{rate ? rate : 0}  </Text>
                        <Image source={require('../../../image/start.png')} style={{ height: 10, width: 10, }} />
                    </View>
                    <Text style={{ fontSize: 11, color: 'red', marginTop: 2, }}>Giá : {price}</Text>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    cardItems: {
        width: 150,
        height: 200,
        marginRight: 10,
        alignItems:'center',
        padding:10,
        backgroundColor: 'white'
        
        // borderWidth: 1, borderColor: 'grey'
    }
})