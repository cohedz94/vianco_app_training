import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, ActivityIndicator, Modal } from 'react-native';
import { connect } from 'react-redux';
import { Router, Scene, Actions, DefaultRenderer, Switch } from 'react-native-router-flux';
import api from '../api/index';
import Loading from './loading';
import Message from './message';

class newRN extends Component {
  _renderItem(state) {
    switch (state) {
      case 'showLoading':
        return (
          <Modal
            transparent={true}
            animationType={'fade'}
            onRequestClose={() => { }}
            visible={true}
          >
            <View
              style={styles.container}
              onPress={() => { api.hideMessage() }}>
              <ActivityIndicator size='large' style={{ alignSelf: 'center' }} />
            </View>
          </Modal>
        )
    }
  }
  render() {
    const state = this.props.UIstate.status;
    console.log(this.props.UIstate)
    console.log(state)
    return (
      <View style={styles.container}>
        {this._renderItem(state)}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: 'rgba(52,52,52,0.5)',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

function mapStateToProps(state) {
  return { UIstate: state.UIstate }
}
export default connect(mapStateToProps)(newRN);