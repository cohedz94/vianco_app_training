import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Modal,
} from 'react-native';
import { Button } from 'native-base'
import api from '../api/index';
import { connect } from 'react-redux'
class MSG extends Component {
    render() {
        let { title, content, showMessage } = this.props.UIstate
        return (
            <Modal
                transparent={true}
                animationType={'fade'}
                onRequestClose={() => { }}
                visible={showMessage}
            >
                <View
                    style={{flex:1, justifyContent:'center', alignItems:'center'}}>
                    <Text>{title}</Text>
                    <Text>{content}</Text>
                </View>
            </Modal>
        );
    }
}

mapStateToProps = (state) => ({ UIstate: state.UIstate })
export default connect(mapStateToProps)(MSG)