import React, { Component } from 'react';
import { View, TextInput, StyleSheet, Image, TouchableOpacity, Text } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { PagerTitleIndicator, IndicatorViewPager } from 'rn-viewpager';

import api from '../../api/index';
import { login } from './login';
import { register } from './register';

class Login extends Component {
    constructor(props) {
        super(props)
        this.state = { user: '', pass: '' }
    }
    static navigationOptions = {
        title: 'Đăng nhập / Đăng ký',
        headerLeft: null,
        headerTintColor: 'white',
        headerStyle: {
            backgroundColor: api.colorPrimary(),
        },
    }
    _renderTitleIndicator() {
        return (
            <PagerTitleIndicator
                itemStyle={{ backgroundColor: 'white' }}
                itemTextStyle={{ color: 'grey' }}
                selectedBorderStyle={{ backgroundColor: api.colorPrimary() }}
                selectedItemTextStyle={{ color: api.colorPrimary() }}
                titles={['Đăng nhập', 'Đăng ký']}>
            </PagerTitleIndicator>
        )
    }
    render() {
        return (
            <View style={{ flex: 1, backgroundColor: 'white' }}>
                <IndicatorViewPager
                    style={{ flex: 1, flexDirection: 'column-reverse' }}
                    indicator={this._renderTitleIndicator()}>
                    {login()}
                    {register()}
                </IndicatorViewPager>
            </View >
        );
    }
}

const styles = StyleSheet.create({
    btn: {
        backgroundColor: 'steelblue',
        justifyContent: 'center',
        alignItems: 'center',
        width: (api.width - 52) / 2,
        height: 40
    }
})

const mapStateToProps = (state) => ({
    UIstate: state.UIstate
})

export default connect(mapStateToProps)(Login);