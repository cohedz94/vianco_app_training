import React, { Component } from 'react';
import { Text, View, StyleSheet, TextInput, TouchableOpacity } from 'react-native';
import api from '../../api';
import {Actions} from 'react-native-router-flux';

const width = api.width - 32;
export function register() {
    return (
        <View style={styles.container}>
            <View style={{ flex: 1 }}></View>
            <View>
                <View style={{ marginBottom: 20 }}>
                    <Text style={{ color: 'grey', marginBottom: 5 }}>Số điện thoại</Text>
                    <TextInput
                        underlineColorAndroid='transparent'
                        style={styles.input}
                    ></TextInput>
                </View>
                <View style={{ marginBottom: 20 }}>
                    <Text style={{ color: 'grey', marginBottom: 5 }}>Số điện thoại</Text>
                    <TextInput
                        underlineColorAndroid='transparent'
                        style={styles.input}
                    ></TextInput>
                </View>
                <View style={{ marginBottom: 20 }}>
                    <Text style={{ color: 'grey', marginBottom: 5 }}>Số điện thoại</Text>
                    <TextInput
                        underlineColorAndroid='transparent'
                        style={styles.input}
                    ></TextInput>
                </View>
            </View>
            <View style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'space-around'
            }}>
                <Text style={{ color: 'grey' }}>Lựa chọn người sử dụng</Text>
                <View style={{
                    borderWidth: 0.5
                    , borderColor: 'grey',
                    flexDirection: 'row',
                    alignItems: 'center',
                    padding:3
                }}>
                    <TouchableOpacity>
                        <Text style={{
                            padding: 5,
                            backgroundColor: api.colorPrimary()
                        }}>Khách hàng lẻ</Text>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Text>Khách hàng đại lý</Text>
                    </TouchableOpacity>
                </View>
            </View>
            <View style={{ flex: 3 }}>
                <TouchableOpacity 
                    onPress={() => {
                        api.showMessage('Đăng kí thành công', 'Email là')}}
                    style={styles.btn}>
                    <Text style={{ color: 'white' }}>Đăng ký</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        flex: 1,
        alignItems: 'center'
    },
    input: {
        width: width,
        borderWidth: 1,
        borderColor: 'grey',
        padding: 5,
    },
    forget: {
        textDecorationLine: 'underline',
        color: api.colorPrimary()
    },
    btn: {
        marginTop: 30,
        alignItems: 'center',
        backgroundColor: api.colorPrimary(),
        borderRadius: 3,
        padding: 10,
        width: width
    }
})
