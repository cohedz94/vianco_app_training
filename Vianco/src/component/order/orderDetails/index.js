import React, { Component } from 'react';
import { Text, View, TouchableOpacity, ScrollView } from 'react-native';
import { Icon } from 'native-base';
import { Actions } from 'react-native-router-flux';

export default class OrderDetails extends Component {
    static navigationOptions = {
        title: 'Đơn hàng',
        headerTintColor: 'white',
        headerStyle: {
            backgroundColor: 'steelblue'
        },
        headerLeft: (
            <TouchableOpacity
                onPress={() => Actions.pop()}>
                <Icon name='arrow-back' style={{ margin: 16, color: 'white' }} />
            </TouchableOpacity>
        )
    }
    _renderRow(header, content) {
        return (
            <View style={{ flexDirection: 'row', padding: 7 }}>
                <Text style={{ fontWeight: 'bold', fontSize: 13, color: 'grey' }}>• {header}:  </Text>
                <Text style={{ fontSize: 13, color: 'grey' }}>{content}</Text>
            </View>
        )
    }
    render() {
        return (
            <ScrollView>
            <View style={{ flex: 1 }}>
                <View style={{
                    backgroundColor: 'white',
                    padding: 16,
                    marginBottom: 1
                }}>
                    <Text>Thông tin đơn hàng</Text>
                </View>
                <View style={{ padding: 16, marginBottom: 5, backgroundColor: 'white' }}>
                    {this._renderRow('Mã đơn hàng', data.id)}
                    {this._renderRow('Tổng hóa đơn', data.sum)}
                    {this._renderRow('Khuyến mại', data.discount)}
                    {this._renderRow('Phương thức thanh toán', data.type)}
                    {this._renderRow('Ngày đặt', data.date)}
                    {this._renderRow('Ngày giao', data.sendDate)}
                </View>
                <View style={{
                    backgroundColor: 'white',
                    padding: 16,
                    marginBottom: 1,
                    flexDirection: 'row',
                    justifyContent: 'space-around'
                }}>
                    <Text>Sản phẩm</Text>
                    <Text>Tiền</Text>
                </View>
                <View style={{
                    backgroundColor: 'white',
                    padding: 16,
                    marginBottom: 5,
                }}>
                </View>
                <View style={{
                    backgroundColor: 'white',
                    padding: 16,
                    marginBottom: 1,
                }}>
                    <Text>Thông tin khách hàng</Text>
                </View>
                <View style={{
                    backgroundColor: 'white',
                    padding: 16,
                    marginBottom: 1,
                }}>
                    {this._renderRow('Email', data.email)}
                    {this._renderRow('Số điện thoại', data.phone)}
                    {this._renderRow('Khách hàng', data.customer)}
                    {this._renderRow('Địa chỉ', data.address)}
                </View>
            </View>
            </ScrollView>
        )
    }
}

const data = {
    id: '007',
    sum: '1.000.000 vnd',
    discount: '10.000 vnd',
    type: 'Chuyển khoản ngân hàng',
    date: '07/07/2017',
    sendDate: '08/07/2017',

    email: 'design.neonstudio@gmail.com',
    phone: '0912 123 456',
    customer: 'Đại lý',
    address: 'số 9, Dương Đình Nghệ, Cầu Giấy, Hà Nội'
}