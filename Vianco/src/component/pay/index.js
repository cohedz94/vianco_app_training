import React, { Component } from 'react';
import { View, FlatList, StyleSheet, Image, Text, TouchableOpacity, TextInput } from 'react-native';
import PayItem from './item/index';
import { Icon } from 'native-base';
import { Actions } from 'react-native-router-flux';
import api from '../../api';

export default class Pay extends Component {
    static navigationOptions = {
        title: 'Giỏ hàng',
        headerTintColor: 'white',
        headerStyle: {
            backgroundColor: 'steelblue'
        },
        headerLeft: (
            <TouchableOpacity
                onPress={() => Actions.pop()}>
                <Icon name='arrow-back' style={{ margin: 16, color: 'white' }} />
            </TouchableOpacity>
        )
    }
    _renderItem(item) {
        return (
            <View style={{ flex: 1, flexDirection: 'row' }}>
                <Image style={{ flex: 1, padding: 10, backgroundColor: 'grey' }}></Image>
                <View style={{ flex: 2 }}>
                    <Text style={{ fontWeight: 'bold' }}>{item.name}</Text>
                    <View style={{ flexDirection: 'row' }}>
                        <Text>Giá:  </Text>
                        <Text>{item.price}</Text>
                        <Text>{item.quantity}</Text>
                    </View>
                </View>
                <View style={{ flex: 1 }}>
                    <Text style={{ color: 'red' }}>X</Text>
                </View>
            </View>
        )
    }
    render() {
        return (
            <View style={{ justifyContent: 'space-around', flex: 1 }}>
                <FlatList
                    style={{ marginBottom: 3 }}
                    data={listItems}
                    renderItem={({ item }) => <PayItem item={item} />}>
                </FlatList>
                <View style={{
                    backgroundColor: 'white',
                    marginBottom: 1,
                    paddingLeft: 16,
                    paddingRight: 16,
                    justifyContent: 'center',
                    height: 48,
                    flexDirection: 'row',
                    justifyContent: 'space-between'
                }}>
                    <TextInput
                        underlineColorAndroid='transparent'
                        placeholder='Nhập mã ưu đãi'></TextInput>
                    <Text style={{
                        padding: 5,
                        paddingLeft: 30,
                        paddingRight: 30,
                        color: 'white',
                        alignSelf: 'center',
                        backgroundColor: api.colorPrimary()
                    }}>Gửi</Text>
                </View>
                <View style={{ height: 56, backgroundColor: 'white', flexDirection: 'row' }}>
                    <View style={{ flex: 1, justifyContent: 'center', }}>
                        <View style={{
                            padding: 3,
                            alignItems: 'center',
                            flexDirection: 'row',
                            alignSelf: 'center'
                        }}>
                            <Text style={{color:'grey'}}>Tổng: </Text>
                            <Text style={{color:'grey'}}>15.000.000 vnd</Text>
                        </View>
                        <View style={{
                            padding: 3,
                            alignSelf: 'center',
                            alignItems: 'center',
                            flexDirection: 'row',
                        }}>
                            <Text style={{color:'grey'}}>Giảm: </Text>
                            <Text style={{color:'grey'}}>- 15.000.000 vnd</Text>
                        </View>
                    </View>
                    <TouchableOpacity 
                        onPress={() => api.showLoading()}
                        style = {{
                        flex: 1,
                        backgroundColor: 'red',
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}>
                        <Text style={{ color: 'white' }}>Đặt mua</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    btn: {
        borderRadius: 20,
        backgroundColor: 'steelblue',
        padding: 10,
        paddingLeft: 20,
        paddingRight: 20,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 20,
        shadowColor: '#000000',
        shadowOpacity: 0.8,
        shadowRadius: 2,
        shadowOffset: {
            height: 1,
            width: 0
        },
        elevation: 2
    }
})
const listItems = [
    {
        key: 1,
        name: 'Bóng Compact xoắn 24W',
        price: '50.000 vnđ',
        sale: '47.000 vnđ',
        quantity: 2,
        sum: '100.000 vnđ,',
        light: 'Trắng',
        type: 'Xoáy/Cài',
        from: 'Việt Nam',
        model: 'CFL Xoắn 24W',
        lumen: '1025 Lm',
        ra: '> 80, 6500K',
        life: '8000h',
        efficiency: '60.28lm/W',
        ac: '150 - 240V, 50Hz',
        listImage: [
            'http://ducthinhlighting.com/kcfinder/upload/images/bong-den-compact-rang-dong-xoan-50w.jpg',
            'http://ducthinhlighting.com/kcfinder/upload/images/bong-den-compact-rang-dong-xoan-50w.jpg',
            'http://ducthinhlighting.com/kcfinder/upload/images/bong-den-compact-rang-dong-xoan-50w.jpg',
        ]
    },
    {
        key: 2,
        name: 'Bóng Compact xoắn 24W',
        price: '50.000 vnđ',
        sale: '47.000 vnđ',
        quantity: 2,
        sum: '100.000 vnđ,',
        light: 'Trắng',
        type: 'Xoáy/Cài',
        from: 'Việt Nam',
        model: 'CFL Xoắn 24W',
        lumen: '1025 Lm',
        ra: '> 80, 6500K',
        life: '8000h',
        efficiency: '60.28lm/W',
        ac: '150 - 240V, 50Hz',
        listImage: [
            'http://ducthinhlighting.com/kcfinder/upload/images/bong-den-compact-rang-dong-xoan-50w.jpg',
            'http://ducthinhlighting.com/kcfinder/upload/images/bong-den-compact-rang-dong-xoan-50w.jpg',
            'http://ducthinhlighting.com/kcfinder/upload/images/bong-den-compact-rang-dong-xoan-50w.jpg',
        ]
    },
    {
        key: 3,
        name: 'Bóng Compact xoắn 24W',
        price: '50.000 vnđ',
        sale: '47.000 vnđ',
        quantity: 2,
        sum: '100.000 vnđ,',
        light: 'Trắng',
        type: 'Xoáy/Cài',
        from: 'Việt Nam',
        model: 'CFL Xoắn 24W',
        lumen: '1025 Lm',
        ra: '> 80, 6500K',
        life: '8000h',
        efficiency: '60.28lm/W',
        ac: '150 - 240V, 50Hz',
        listImage: [
            'http://ducthinhlighting.com/kcfinder/upload/images/bong-den-compact-rang-dong-xoan-50w.jpg',
            'http://ducthinhlighting.com/kcfinder/upload/images/bong-den-compact-rang-dong-xoan-50w.jpg',
            'http://ducthinhlighting.com/kcfinder/upload/images/bong-den-compact-rang-dong-xoan-50w.jpg',
        ]
    },
    {
        key: 4,
        name: 'Bóng Compact xoắn 24W',
        price: '50.000 vnđ',
        sale: '47.000 vnđ',
        quantity: 2,
        sum: '100.000 vnđ,',
        light: 'Trắng',
        type: 'Xoáy/Cài',
        from: 'Việt Nam',
        model: 'CFL Xoắn 24W',
        lumen: '1025 Lm',
        ra: '> 80, 6500K',
        life: '8000h',
        efficiency: '60.28lm/W',
        ac: '150 - 240V, 50Hz',
        listImage: [
            'http://ducthinhlighting.com/kcfinder/upload/images/bong-den-compact-rang-dong-xoan-50w.jpg',
            'http://ducthinhlighting.com/kcfinder/upload/images/bong-den-compact-rang-dong-xoan-50w.jpg',
            'http://ducthinhlighting.com/kcfinder/upload/images/bong-den-compact-rang-dong-xoan-50w.jpg',
        ]
    },
]