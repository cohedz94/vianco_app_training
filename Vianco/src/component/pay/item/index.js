import React, { Component } from 'react';
import { View, Image, Text, TouchableOpacity, StyleSheet } from 'react-native';
import { Icon } from 'native-base';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import api from '../../../api/index';

export default class PayItem extends Component {
    constructor(props) {
        super(props)
    }
    render() {
        const item = this.props.item;
        const width = api.width;
        const height = api.height / 5;
        return (
            <View style={{
                flexDirection: 'row',
                marginBottom: 1,
                width: width,
                height: height,
                backgroundColor: 'white'
            }}>
                <Image
                    resizeMode='contain'
                    source={require('../../../image/led.jpeg')}
                    style={{
                        alignSelf: 'center',
                        width: '25%',
                        height: '75%'
                    }}></Image>
                <View style={{
                    paddingTop: 16,
                    paddingBottom: 16,
                    flex: 3,
                    justifyContent: 'space-around'
                }}>
                    <Text style={{
                        fontWeight: 'bold',
                        color: 'grey'
                    }}>{item.name}</Text>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={{ color: 'grey' }}>Giá sỉ: </Text>
                        <Text style={{ color: 'red' }}>{item.sale}</Text>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={{ color: 'grey' }}>Giá lẻ: </Text>
                        <Text style={{ color: 'red' }}>{item.price}</Text>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={{color:'grey'}}> Số lượng: </Text>
                        <MaterialIcon
                            name='remove-circle-outline'
                            style={{ fontSize: 20, color: 'grey' }} />
                        <Text style={{
                            fontSize: 15,
                            color: 'red',
                            marginLeft: 10,
                            marginRight: 10
                        }}>{item.quantity}</Text>
                        <MaterialIcon
                            name='add-circle-outline'
                            style={{ fontSize: 20, color: 'grey' }} />
                    </View>
                </View>
                <View style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center'
                }}>
                    <Icon
                        style={{ fontSize: 24, color: 'red' }}
                        name='backspace' />
                </View>
            </View>
        );
    }
}