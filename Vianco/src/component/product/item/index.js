import React, { Component } from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';
import { Card } from 'native-base';

export default class ProductItems extends Component {
    render() {
        return (
            <View style={styles.cardItems}>
                <Image
                    source={require('../../../image/led.jpeg')}
                    style={{ width: 75, height: 100, backgroundColor: 'grey', marginBottom: 10, alignSelf:'center' }}></Image>
                <View style={{ flex: 1, justifyContent:'center', }}>
                    <Text style={{ color: 'grey', fontSize: 12 }}>Bóng Compact EU xoắn 24W</Text>
                    <Text style={{ fontSize: 12, color: 'red', marginTop:10, }}>50.000 vnđ</Text>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    cardItems: {
        width: 150,
        padding: 10,
        marginLeft:1,
        backgroundColor:'white'
    }
})