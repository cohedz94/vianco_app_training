import React, { Component } from 'react';
import { View, TextInput, Image, TouchableOpacity, StyleSheet, Text } from 'react-native';
import { Icon } from 'native-base';
import { Actions } from 'react-native-router-flux';
import api from '../../api';

export default class ProfileInformation extends Component {
    static navigationOptions = {
        title: 'Tài khoản',
        headerTintColor: 'white',
        headerStyle: {
            backgroundColor: api.colorPrimary()
        },
        headerLeft: (
            <TouchableOpacity
                onPress={() => Actions.pop()
                }>
                <Icon name='arrow-back' style={{ margin: 16, color: 'white' }} />
            </TouchableOpacity>
        )
    }
    _renderRows(header, content) {
        return (
            <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', marginLeft:16, marginRight:16}}>
                <Text style={{ fontWeight: 'bold', color: 'grey' }}>{header}:  </Text>
                <Text style={{ color: 'grey' }}>{content}</Text>
            </View>
        )
    }
    render() {
        return (
            <View style={{ flex: 1, justifyContent:'space-between' }}>
                <View style={{ flex: 4, backgroundColor: api.colorPrimary(), alignItems: 'center', justifyContent: 'center' }}>
                    <Image
                        source={{uri: 'https://scontent.fhan4-1.fna.fbcdn.net/v/t31.0-1/c379.0.1290.1290/10506738_10150004552801856_220367501106153455_o.jpg?oh=a0a4f734bc9ddaef9c44b86e29785fce&oe=5AA69D7C'}}
                        style={{ width: 120, height: 120, margin: 10, borderRadius: 60, alignSelf: 'center', marginRight: 16 }}></Image>
                    <Text style={{ color: 'white', fontSize: 20, fontWeight: 'bold', }}>Quang Huy</Text>
                    <Text style={{ fontSize: 14, paddingTop: 5, marginBottom: 20, fontStyle: 'italic', color: 'white' }}>Đại lý</Text>
                </View>
                <View style={{ flex: 6 }}>
                    {this._renderRows('Giới tính', 'Nam')}
                    {this._renderRows('Tuổi', '23')}
                    {this._renderRows('Tỉnh/thành', 'Hà Nội')}
                    {this._renderRows('Quận/huyện', 'Cầu Giấy')}
                    {this._renderRows('Số nhà', '123')}
                    {this._renderRows('Số điện thoại', '090123456789')}
                </View>
                <View style={{ flex: 1 }}>
                    <TouchableOpacity style={styles.btn}>
                        <Text style={{color:'white'}}>Cập nhật thông tin</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    btn: {
        flex:1,
        backgroundColor: api.colorPrimary(),
        justifyContent: 'center',
        alignItems: 'center',
    }
})