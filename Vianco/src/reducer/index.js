import { combineReducers, applyMiddleware } from 'redux';
import UIstate from './uiReducer';
import { createStore } from 'redux'
import { createLogger } from 'redux-logger'
let root = combineReducers({ UIstate })
let store = createStore(root, applyMiddleware(createLogger()));
export default store