import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, ActivityIndicator, Modal, } from 'react-native';
import { connect } from 'react-redux';
import { Router, Scene, Actions, DefaultRenderer, Switch } from 'react-native-router-flux';
import { CheckBox } from 'react-native-elements';
import api from '../api/index';

class newRN extends Component {
  constructor(props) {
    super(props);
    this.state = {
      checked: false
    }
  }
  _renderItem(type) {
    switch (type) {
      case 'loading':
        return (
          <Modal
            transparent={true}
            animationType={'fade'}
            onRequestClose={() => { }}
            visible={true}
          >
            <View
              style={styles.container}>
              <ActivityIndicator size='large' style={{ alignSelf: 'center' }} />
            </View>
          </Modal>
        )
      case 'message':
        return (
          <View style={styles.msgbox}>
            <Text style={{
              fontSize: 16,
              color: api.colorPrimary()
            }}>
              Thanh toán thành công
            </Text>
            <TouchableOpacity
              onPress={() => Actions.pop()}
              style={styles.btn}>
              <Text style={{
                color: 'white'
              }}>
                OK
              </Text>
            </TouchableOpacity>
          </View>
        )
      case 'typeCheckin':
        return (
          <View style={{
            width: 300,
            height: 125,
            backgroundColor: 'white',
            borderRadius: 3,
            justifyContent:'center'}}>
            <CheckBox
              containerStyle={{ backgroundColor: 'white', borderWidth: 0, alignSelf:'flex-start' }}
              center
              title='Sử dụng thông tin mặc định'
              checkedIcon='dot-circle-o'
              uncheckedIcon='circle-o'
              checkedColor={api.colorPrimary()}
              checked={this.state.checked}
              onPress={() => this.setState(previousState => {
                return { checked: !previousState.checked };
              })}
            />
            <CheckBox
              containerStyle={{ backgroundColor: 'white', borderWidth: 0,  alignSelf:'flex-start' }}
              center
              title='Nhập thông tin thanh toán mới'
              checkedIcon='dot-circle-o'
              uncheckedIcon='circle-o'
              checkedColor={api.colorPrimary()}
              checked={!this.state.checked}
              onPress={() => this.setState(previousState => {
                return { checked: !previousState.checked };
              })}
            />
          </View>
        )
    }
  }
  render() {
    return (
      <View style={styles.container}>
        {this._renderItem(this.props.data)}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: 'rgba(52,52,52,0.5)',
    justifyContent: 'center',
    alignItems: 'center',
  },
  msgbox: {
    width: 300,
    height: 125,
    backgroundColor: 'white',
    borderRadius: 3,
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  btn: {
    borderRadius: 2,
    paddingLeft: 30,
    paddingRight: 30,
    padding: 5,
    backgroundColor: api.colorPrimary()
  }
});

function mapStateToProps(state) {
  return { UIstate: state.UIstate }
}
export default connect(mapStateToProps)(newRN);