import React, { Component } from 'react';
import { Text, View, TouchableOpacity, StyleSheet } from 'react-native';
import {Actions} from 'react-native-router-flux';

export default class Manager extends Component {
    static navigationOptions = {
        title: 'Trang chủ',
        headerLeft:
        <View>
            {Button.home(24, 'white')}
        </View>,
        headerTintColor: 'white',
        headerRight:
        <View style={{ flexDirection: 'row' }}>
            {Button.search(24, 'white')}
            {Button.cart(24, 'white')}
        </View>,
        headerStyle: {
            backgroundColor: api.colorPrimary(),
        },
    }
    render() {
        return (
            <View style={{ flex: 1 }}>
                <TouchableOpacity
                    style={styles.list}
                >
                    <Text
                        style={styles.txtContent}>
                        Thông tin tài khoản
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={styles.list}
                    onPress={()=> Actions.listorder()}
                >
                    <Text
                        style={styles.txtContent}>
                        Quản lý đơn hàng
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={styles.list}
                >
                    <Text
                        style={styles.txtContent}>
                        Thông báo
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={styles.list}
                >
                    <Text
                        style={styles.txtContent}>
                        Sản phẩm đã xem
                    </Text>
                </TouchableOpacity>
                <View style={{ flex: 1, backgroundColor: 'white' }}></View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    list: {
        marginBottom: 1,
        backgroundColor: 'white',
        padding: 16
    },
    txtContent: {
        color: 'grey'
    }
})