import React, { Component } from 'react'
import { Text, View, TextInput, TouchableOpacity } from 'react-native';
import { Icon } from 'native-base';
import {Actions} from 'react-native-router-flux';

export default class Checkin extends Component {
    static navigationOptions = {
        title: 'Checkin',
        headerLeft:
        <TouchableOpacity
            onPress={() => Actions.drawerOpen()}>
            <Icon style={{ fontSize: 24, color: 'white', padding: 16 }} name='menu'>
            </Icon>
        </TouchableOpacity>,
        headerTintColor: 'white',
        headerStyle: {
            backgroundColor: api.colorPrimary(),
        },
    }
    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={{
                    height: 48,
                    backgroundColor: 'white',
                    justifyContent: 'center',
                    paddingLeft: 16,
                    paddingRight: 16,
                    marginBottom:1
                }}>
                    <TextInput
                        placeholder='Nhập số lượng hàng tồn của cửa hàng'></TextInput>
                </View>
                <View style={{flex:1, padding:16, backgroundColor:'white'}}>
                    <TextInput
                        placeholder='Ghi chú'></TextInput>
                </View>
                <View style={{
                    flexDirection: 'row',
                    height: 48,
                    justifyContent:'space-around',
                    alignItems:'center'
                }}>
                    <Text style={{
                        color: api.colorPrimary()
                    }}>GPS</Text>
                    <Text style={{
                        color: api.colorPrimary()
                    }}>Hình ảnh</Text>
                </View>
                <TouchableOpacity
                    style={{
                        height:56,
                        backgroundColor: api.colorPrimary(),
                        justifyContent:'center',
                        alignItems:'center'}}>
                    <Text style={{
                        color:'white',
                        fontSize:16}}>Checkin</Text>
                </TouchableOpacity>
            </View>
        )
    }
}