import React, { Component } from 'react';
import { View, TextInput, TouchableOpacity, Text, StyleSheet, FlatList, ScrollView } from 'react-native';
import { Icon, Card } from 'native-base';
import api from '../../api/index';
import { Actions } from 'react-native-router-flux';

export default class Order extends Component {
    static navigationOptions = {
        title: 'Danh sách đơn hàng',
        headerTintColor: 'white',
        headerStyle: {
            backgroundColor: 'steelblue'
        },
        headerLeft: (
            <TouchableOpacity
                onPress={() => Actions.pop()}>
                <Icon name='menu' style={{ margin: 16, color: 'white' }} />
            </TouchableOpacity>
        )
    }
    _renderButtons(isDelivered) {
        if (!isDelivered) {
            return (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: 'white', marginBottom: 1 }}>
                    <TouchableOpacity
                        style={styles.btn}>
                        <Text style={{ color: 'white' }}>Chi tiết</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.btn}>
                        <Text style={{ color: 'white' }}>Hủy</Text>
                    </TouchableOpacity>
                </View>
            )
        }
    }
    _renderRows(string, content) {
        return (
            <View style={{ flexDirection: 'row', padding: 7 }}>
                <Text style={{ fontWeight: 'bold', color: 'grey' }}>{string}:  </Text>
                <Text style={{ color: 'grey' }}>{content}</Text>
            </View>
        )
    }
    render() {
        return (
            <ScrollView style={{ flex: 1 }}>
                <View style={{ flex: 1, height: 56, flexDirection: 'row', marginBottom:1 }}>
                    <View style={{
                        flex: 1,
                        borderColor: 'grey',
                        marginRight: 1,
                        backgroundColor: 'white',
                        alignItems: 'center',
                        flexDirection: 'row',
                        justifyContent: 'space-around'
                    }}>
                        <TextInput placeholder='Tìm kiếm'></TextInput>
                        <Icon style={{ fontSize: 24, color:'grey' }} name='search' />
                    </View>
                    <View style={{
                        flex: 1,
                        borderColor: 'grey',
                        backgroundColor: 'white',
                        alignItems: 'center',
                        flexDirection: 'row',
                        justifyContent: 'space-around'
                    }}>
                        <Text style={{color:'grey'}}>Xắp xếp</Text>
                        <Icon style={{ fontSize: 24, color:'grey' }} name='arrow-down' />
                    </View>
                </View>
                <View style={{ flex: 7 }}>
                    <FlatList
                        data={orders}
                        renderItem={({ item }) =>
                            <View style={{ flexDirection: 'row', }}>
                                <View style={{ flex: 2, backgroundColor: 'white', marginBottom: 1, padding:16 }}>
                                    {this._renderRows('Mã đơn hàng', item.id)}
                                    {this._renderRows('Ngày', item.date)}
                                    {this._renderRows('Sản phẩm', item.product)}
                                    {this._renderRows('Thành tiền', item.price)}
                                </View>
                                {this._renderButtons(item.isDelivered)}
                            </View>}>
                    </FlatList>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    btn: {
        backgroundColor: '#008fd4',
        width: 100,
        height: 35,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 10,
        borderRadius: 3
    }
})

const orders = [
    {
        key: 0,
        id: '007',
        date: '07/07/2017',
        product: '2 đèn LED EU 24W',
        price: '100.000 vnđ',
        isDelivered: true
    },
    {
        key: 1,
        id: '007',
        date: '07/07/2017',
        product: '2 đèn LED EU 24W',
        price: '100.000 vnđ',
        isDelivered: true
    },
    {
        key: 2,
        id: '007',
        date: '07/07/2017',
        product: '2 đèn LED EU 24W',
        price: '100.000 vnđ',
        isDelivered: false
    },
    {
        key: 3,
        id: '007',
        date: '07/07/2017',
        product: '2 đèn LED EU 24W',
        price: '100.000 vnđ',
        isDelivered: false
    },
    {
        key: 4,
        id: '007',
        date: '07/07/2017',
        product: '2 đèn LED EU 24W',
        price: '100.000 vnđ',
        isDelivered: false
    },
    {
        key: 5,
        id: '007',
        date: '07/07/2017',
        product: '2 đèn LED EU 24W',
        price: '100.000 vnđ',
        isDelivered: false
    },
    {
        key: 6,
        id: '007',
        date: '07/07/2017',
        product: '2 đèn LED EU 24W',
        price: '100.000 vnđ',
        isDelivered: false
    },
]