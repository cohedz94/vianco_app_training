import React from 'react';
import { Text, View, ScrollView, } from 'react-native';

function renderRow(header, content){
    return (
        <View style={{ flexDirection: 'row', padding: 7 }}>
            <Text style={{ fontWeight: 'bold', fontSize: 13, color: 'grey' }}>• {header}:  </Text>
            <Text style={{ fontSize: 13, color: 'grey' }}>{content}</Text>
        </View>
    )
}

export function info(data) {
    return (
        <View style={{flex:1}}>
        <ScrollView>
            <View style={{ flex: 1 }}>
                <View style={{
                    backgroundColor: 'white',
                    padding: 16,
                    marginBottom: 1
                }}>
                    <Text>Thông tin đơn hàng</Text>
                </View>
                <View style={{ padding: 16, marginBottom: 5, backgroundColor: 'white' }}>
                    {renderRow('Mã đơn hàng', data.id)}
                    {renderRow('Tổng hóa đơn', data.sum)}
                    {renderRow('Khuyến mại', data.discount)}
                    {renderRow('Phương thức thanh toán', data.type)}
                    {renderRow('Ngày đặt', data.date)}
                    {renderRow('Ngày giao', data.sendDate)}
                </View>
                <View style={{
                    backgroundColor: 'white',
                    padding: 16,
                    marginBottom: 1,
                    flexDirection: 'row',
                    justifyContent: 'space-around'
                }}>
                    <Text>Sản phẩm</Text>
                    <Text>Tiền</Text>
                </View>
                <View style={{
                    backgroundColor: 'white',
                    padding: 16,
                    marginBottom: 5,
                }}>
                </View>
                <View style={{
                    backgroundColor: 'white',
                    padding: 16,
                    marginBottom: 1,
                }}>
                    <Text>Thông tin khách hàng</Text>
                </View>
                <View style={{
                    backgroundColor: 'white',
                    padding: 16,
                    marginBottom: 1,
                }}>
                    {renderRow('Email', data.email)}
                    {renderRow('Số điện thoại', data.phone)}
                    {renderRow('Khách hàng', data.customer)}
                    {renderRow('Địa chỉ', data.address)}
                </View>
            </View>
        </ScrollView>
        </View>
    )
}