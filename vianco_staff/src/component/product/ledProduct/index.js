import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, FlatList } from 'react-native';
import { Icon } from 'native-base';
import ProductItems from '../item';
import { Actions } from 'react-native-router-flux';

export default class LedProduct extends Component {
    constructor(props) {
        super(props)
        this.state = {
            starCount: 0,
            nameProduct: 'Bóng Compact xoắn 24W',
            price: '50.000 vnđ',
            sale: '47.000 vnđ',
            quantity: 2,
            sum: '100.000 vnđ,',
            light: 'Trắng',
            type: 'Xoáy/Cài',
            from: 'Việt Nam',
            model: 'CFL Xoắn 24W',
            lumen: '1025 Lm',
            ra: '> 80, 6500K',
            life: '8000h',
            efficiency: '60.28lm/W',
            ac: '150 - 240V, 50Hz',
            listImage: [
                'https://img.websosanh.vn/v2/users/root_product/images/FXJFkn5JEdmI.jpg?width=200&compress=85',
                'https://img.websosanh.vn/v2/users/root_product/images/FXJFkn5JEdmI.jpg?width=200&compress=85',
                'https://img.websosanh.vn/v2/users/root_product/images/FXJFkn5JEdmI.jpg?width=200&compress=85',
            ],
            content: 'Bóng đèn Compact EUROSUPER - EUROSTAR được sản xuất tại Việt Nam với dây chuyền hiện đại theo tiêu chuẩn công nghệ Châu Âu được người tiêu dùng bình chọn.',
            tut: [
                'Lắp bóng đèn vào đuôi đèn. Lưu ý không lắp bóng đèn khi công tắc điện vào đèn đang bật để an toàn khi lắp bóng',
                'Không dùng với công tắc điều chỉnh độ sáng',
                'Không lắp bóng đèn ở khu vực ngoài trời có tiếp xúc' + '\r\n' + 'trực tiếp với nước và độ ẩm cao.',
                'Lắp bóng đèn xa nguồn nhiệt để bảo vệ linh kiện điện tử.'
            ]
        }
    }
    static navigationOptions = {
        title: 'Đèn LED',
        headerTintColor: 'white',
        headerStyle: {
            backgroundColor: api.colorPrimary()
        },
        headerLeft: (
            <TouchableOpacity
                onPress={() => Actions.pop()
                }>
                <Icon name='arrow-back' style={{ margin: 16, color: 'white' }} />
            </TouchableOpacity>
        )
    }
    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={{ 
                    flexDirection: 'row', 
                    marginBottom: 1, }}>
                    <TouchableOpacity
                        onPress={() => Actions.filter()}
                        style={{ flex: 2.5, marginRight: 1, backgroundColor: 'white', justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
                        <Icon name='funnel' style={{ fontSize: 24, padding: 5 }}></Icon>
                        <Text>Lọc</Text>
                    </TouchableOpacity>
                    <View style={{ flexDirection: 'row', marginRight: 1, backgroundColor: 'white', flex: 2.5, justifyContent: 'center', alignItems: 'center' }}>
                        <Icon name='swap' style={{ fontSize: 24, padding: 5 }}></Icon>
                        <Text>Xắp xếp</Text>
                    </View>
                    <View style={{ flex: 1, backgroundColor: 'white', justifyContent: 'center', alignItems: 'center' }}>
                        <Icon name='grid' style={{ fontSize: 24, padding: 5 }}></Icon>
                    </View>
                </View>
                <FlatList
                    style={{ flex: 1 }}
                    numColumns={2}
                    renderItem={({ item }) =>
                        <View style={{
                            width: (api.width / 2),
                            height: (api.height / 2.5),
                            marginRight: 1,
                            marginBottom: 1,
                            backgroundColor: 'white',
                            justifyContent: 'center',
                            padding: 16
                        }}>
                            <Image
                                resizeMode='contain'
                                style={{ height: 125, marginBottom: 10 }}
                                source={{ uri: 'https://img.websosanh.vn/v2/users/root_product/images/FXJFkn5JEdmI.jpg?width=200&compress=85', }}></Image>
                            <View style={{ flex: 1, justifyContent: 'space-around' }}>
                                <Text style={{ fontSize: 16 }}>{this.state.nameProduct}</Text>
                                <Text style={{ color: 'orange', paddingTop: 10, paddingBottom: 10 }}>4.5 ★</Text>
                                <Text style={{ color: 'red', paddingBottom: 10 }}>{this.state.sale}</Text>
                            </View>
                        </View>}
                    data={[
                        {
                            key: 0
                        },
                        {
                            key: 1
                        },
                        {
                            key: 2
                        },
                        {
                            key: 3
                        },
                        {
                            key: 4
                        },
                        {
                            key: 5
                        },
                        {
                            key: 6
                        },
                        {
                            key: 7
                        },
                        {
                            key: 8
                        }
                    ]}></FlatList>
            </View>
        );
    }
}
